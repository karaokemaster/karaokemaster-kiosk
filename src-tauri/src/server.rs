use tauri::Manager;
use crate::state::ManagerExt;
use anyhow::Result;
use signalrs_client::{hub::Hub, SignalRClient};
use serde::{Serialize, Deserialize};
use tauri::AppHandle;
use signalrs_derive::HubArgument;
use mdns_sd::{ServiceDaemon, ServiceEvent};
use url::Url;
use tokio::sync::Mutex;
use system_shutdown::{logout, shutdown};
use log::*;
use base64::{Engine as _, engine::general_purpose};
use crate::systeminfo::system_info::{encrypt_public, decrypt, get_name, KeyPair};

// the payload type must implement `Serialize` and `Clone`.
#[derive(Clone, Serialize, Deserialize, HubArgument)]
pub struct KioskIdPayload {
    pub id: i32,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct DisplayIdPayload {
    pub id: i32,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StatusPayload {
    pub display_id: i32,
    pub status: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ServerUrlPayload {
    pub hub_url: String,
    pub loki_url: String,
}

#[derive(Serialize, Deserialize, HubArgument)]
#[serde(rename_all = "camelCase")]
pub struct ServerInfo {
    pub api_version: String,
    pub api_release: String,
    pub api_date: String,
    pub api_sha: String
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DisplaySettings {
    pub kiosk_id: i32,
    pub display_id: i32,
    pub display_url: String
}

pub struct ServerConnection {
    app_handle: AppHandle,
    keypair: Mutex<Option<KeyPair>>,
    client: Mutex<Option<SignalRClient>>
}

impl ServerConnection {
    pub fn new(app_handle: AppHandle) -> Self {
        trace!("ServerConnection constructor starting...");

        let the_handle1 = app_handle.clone();
        let the_handle2 = app_handle.clone();

        app_handle.listen_global("set-keypair", move |event| {
            trace!("eventhandler: set-keypair");

            let handle = the_handle1.clone();

            tauri::async_runtime::spawn(async move {
                let keypair: KeyPair = serde_json::from_str::<KeyPair>(event.payload().unwrap()).unwrap();
                let server = handle.get_server_connection().await.unwrap();
                *server.keypair.lock().await = Some(keypair);
            });
        });

        app_handle.listen_global("time-sync", move |_event| {
            trace!("eventhandler: time-sync");

            let handle = the_handle2.clone();

            tauri::async_runtime::spawn(async move {
                let server = handle.get_server_connection().await.unwrap();

                let (hub_url, loki_url) = ServerConnection::get_kiosk_hub_url().await.unwrap();
                let hub = hub_url.clone();
                let payload = ServerUrlPayload { hub_url, loki_url };
                let payload_string = serde_json::to_string(&payload).unwrap();
                handle.trigger_global("set-server-urls", Some(payload_string));

                unsafe {
                    server.connect(hub).await.unwrap();
                }

                handle.trigger_global("connected", None);
            });
        });

        trace!("ServerConnection constructor complete");

        Self {
            app_handle,
            keypair: Default::default(),
            client: Default::default()
        }
    }

    // TODO: Find a "safe" way to do this...
    pub async unsafe fn connect(self: &Self, hub_url: String) -> Result<()> {
        let parsed_url = Url::parse(&hub_url)?;
        let host = if cfg!(feature = "localhost") {
            warn!("Forcing localhost, ignoring discovered hub url {hub_url}");
            "localhost"
        } else {
            parsed_url.host_str().unwrap_or("localhost")
        };
        let mut hub_path = if cfg!(feature = "localhost") {
            "/hubs/kiosk".to_string()
        } else {
            parsed_url.path().to_string()
        };

        hub_path.remove(0);

        info!("host {host} path {hub_path}");

        let keypair_mutex = self.keypair.lock().await;
        let keypair = keypair_mutex.as_ref().expect("KeyPair is null");
        let pubkey_64 = general_purpose::STANDARD.encode(keypair.public_key);

        // TODO: Find a way to wrap in unsafe block
        let owned_self = extend_lifetime::extend_lifetime(self);

        let set_kiosk_callback = |id: i32| ServerConnection::set_kiosk(owned_self, id);
        let configure_display_callback = |settings: String| ServerConnection::configure_display(owned_self, settings);

        let hub = Hub::default()
            .method("ServerInfo", ServerConnection::server_info)
            .method("SetKioskId", set_kiosk_callback)
            .method("ConfigureDisplay", configure_display_callback)
            .method("Logout", ServerConnection::logout)
            .method("Quit", ServerConnection::quit)
            .method("Shutdown", ServerConnection::shutdown)
            ;

        let client = if cfg!(feature = "localhost") {
            SignalRClient::builder(host)
                .use_port(5000)
                .use_hub(hub_path)
                .use_unencrypted_connection()
                .with_client_hub(hub)
                .build()
                .await?
        } else {
            SignalRClient::builder(host)
                .use_hub(hub_path)
                .with_client_hub(hub)
                .build()
                .await?
        };


        let name = get_name().unwrap();
        let (encrypted_key, ephemeral_key) = encrypt_public(pubkey_64);

        client
            .method("Connect")
            .arg(name)?
            .arg(encrypted_key)?
            .arg(ephemeral_key)?
            .invoke_unit()
            .await?;

        // debug!("Got {result}");

        *self.client.lock().await = Some(client);

        Ok(())
    }

    pub async fn get_kiosk_hub_url() -> Result<(String, String)> {
        let mdns = ServiceDaemon::new().expect("Failed to create daemon");

        let service_type = "_karaokemaster._tcp.local.";
        let receiver = mdns.browse(service_type).expect("Failed to browse");

        while let Ok(event) = receiver.recv() {
            match event {
                ServiceEvent::ServiceResolved(info) => {
                    trace!("Resolved a new service: {}", info.get_fullname());
                    let hub_url = info.get_properties().get_property_val_str("kiosk").unwrap();
                    let loki_url = info.get_properties().get_property_val_str("loki").unwrap();

                    trace!("Hub url: {}", hub_url);
                    trace!("Loki url: {}", loki_url);

                    // Shutdown the mDNS daemon
                    mdns.shutdown().unwrap();

                    return Ok((hub_url.to_string(), loki_url.to_string()));
                }
                other_event => {
                    trace!("Received other event: {:?}", &other_event);
                }
            }
        }

        mdns.shutdown().unwrap();

        anyhow::bail!("yikes");
    }

    pub async fn server_info(info: ServerInfo) {
        let version = info.api_version;
        trace!("data {version}");
    }

    pub async fn logout() {
        trace!("Logout requested");
        match logout() {
            Ok(_) => info!("Logging out"),
            Err(e) => error!("Failed to log out: {}", e),
        }
    }

    pub async fn quit() {
        trace!("Quit requested");
        quit::with_code(0);
    }

    pub async fn shutdown() {
        trace!("Shutodwn requested");
        match shutdown() {
            Ok(_) => info!("Shutting down"),
            Err(e) => error!("Failed to shut down: {}", e),
        }
    }

    async fn set_kiosk(self: &Self, id: i32) {
        trace!("kiosk id {id}");

        let payload = serde_json::to_string(&KioskIdPayload { id }).unwrap();
        self.app_handle.trigger_global("set-kiosk-id", Some(payload));
        self.app_handle.emit_all("set-kiosk-id", KioskIdPayload { id }).unwrap();
    }

    async fn configure_display(self: &Self, encrypted_settings: String) {
        let keypair_mutex = self.keypair.lock().await;
        let keypair = keypair_mutex.as_ref().expect("KeyPair is null");

        let windows = self.app_handle.get_window_manager().await.unwrap();

        let settings_json = decrypt(keypair.clone(), encrypted_settings);
        let settings =  serde_json::from_str::<DisplaySettings>(&settings_json).unwrap();

        let display_id: i32 = (settings.display_id) as i32;

        info!("Got settings for display {display_id}");

        windows.set_display_url(display_id, settings.display_url).await;
    }
}