pub mod system_info {
    use tauri::{
        plugin::{Builder, TauriPlugin},
        Manager,
        Runtime,
    };

    use gethostname::gethostname;
    use sodalite::{BoxNonce, BoxPublicKey, BoxSecretKey};
    use mac_address::get_mac_address;
    use rsntp::AsyncSntpClient;
    use chrono::{DateTime, Local, Duration};
    use anyhow::Result;
    use tokio::time::sleep;
    use rand::Rng;
    use log::*;
    use base64::{Engine as _, engine::general_purpose};
    
    #[derive(Clone, serde::Serialize, serde::Deserialize)]
    pub struct KeyPair {
        pub public_key: BoxPublicKey,
        pub secret_key: BoxSecretKey
    }

    impl Default for KeyPair {
        fn default() -> Self {
            Self {
                public_key: BoxPublicKey::default(),
                secret_key: BoxPublicKey::default(),
            }
        }
    }

    static API_PUBLIC_KEY: [u8; 32] = [97,130,189,110,21,109,113,69,21,216,190,161,110,76,96,226,61,35,183,118,118,180,145,210,242,182,159,189,228,235,132,103];

    pub fn plugin<R: Runtime>() -> TauriPlugin<R> {
        Builder::new("systeminfo")
            .setup(|app_handle| {
                let the_handle = app_handle.clone();

                app_handle.listen_global("splash-complete", move |_event| {
                    trace!("eventhandler: splash-complete");

                    let handle = the_handle.clone();

                    tauri::async_runtime::spawn(async move {
                        let keypair = get_keypair().await.unwrap();
                        let payload = serde_json::to_string(&keypair).unwrap();
                        handle.trigger_global("set-keypair", Some(payload));

                        wait_for_time_sync().await.unwrap();
                        info!("time synced");
                        handle.trigger_global("time-sync", None);
                    });
                });

                Ok(())
            })
            .build()
    }

    pub fn get_name() -> Result<String> {
        let hostname = gethostname().into_string().unwrap();
        return Ok(hostname);
    }

    pub async fn get_keypair() -> Result<KeyPair> {
        let public_key: &mut BoxPublicKey = &mut [0; 32];
        let secret_key: &mut BoxSecretKey = &mut [0; 32];

        match get_mac_address() {
            Ok(Some(ma)) => {
                info!("Found MAC address {ma}");
                let b = ma.bytes();
                let hash: &mut sodalite::Hash = &mut [0; 64];
                sodalite::hash(hash, &b);
                let seed: [u8; 32] = hash[0 .. 32].try_into().unwrap();
                sodalite::box_keypair_seed(public_key, secret_key, &seed);

                let keypair: KeyPair = KeyPair { public_key: public_key.clone(), secret_key: secret_key.clone() };
                return Ok(keypair);
            },
            Ok(None) => panic!("Nope"),
            Err(e) => panic!("Error {e}")
        };
    }

    pub fn encrypt_public(data: String) -> (String, String) {
        let ephemeral_public_key: &mut BoxPublicKey = &mut [0; 32];
        let ephemeral_secret_key: &mut BoxSecretKey = &mut [0; 32];

        let seed = rand::thread_rng().gen::<[u8; 32]>();
        sodalite::box_keypair_seed(ephemeral_public_key, ephemeral_secret_key, &seed);

        let nonce: BoxNonce = [0u8; 24];
        let box_key: &mut [u8; 32] = &mut [0; 32];

        // sodalite expects 32x 0 bytes at the beginning of data
        let data_vec = data.into_bytes();
        let data_concat = [&[0u8; 32], data_vec.as_slice()].concat();
        let data_bytes = data_concat.as_slice();

        sodalite::box_beforenm(box_key, &API_PUBLIC_KEY, &ephemeral_secret_key);

        let mut encoded_bytes = Vec::<u8>::new();
        encoded_bytes.resize(data_bytes.len(), 0u8);

        sodalite::box_afternm(&mut encoded_bytes, data_bytes, &nonce, &box_key).unwrap();

        let encoded_text = general_purpose::STANDARD.encode(&encoded_bytes);
        let encoded_key = general_purpose::STANDARD.encode(ephemeral_public_key);

        (encoded_text, encoded_key)
    }

    pub fn decrypt(keypair: KeyPair, data: String) -> String {
        let data_vec = general_purpose::STANDARD.decode(data).unwrap();
        let data_concat = [&[0u8; 16], data_vec.as_slice()].concat();
        let data_bytes = data_concat.as_slice();
        let nonce: BoxNonce = [0u8; 24];

        let mut decoded_bytes = Vec::<u8>::new();
        decoded_bytes.resize(data_bytes.len(), 0u8);

        let _x = sodalite::box_open(&mut decoded_bytes, data_bytes, &nonce, &API_PUBLIC_KEY, &keypair.secret_key);
        let decoded_text = std::str::from_utf8(&decoded_bytes[32..]).unwrap();

        decoded_text.to_string()
    }

    pub async fn local_time() -> Result<DateTime<Local>> {
        let client = AsyncSntpClient::new();
        let result = client.synchronize("pool.ntp.org").await?;

        Ok(DateTime::from(result.datetime().into_chrono_datetime()?))
    }

    pub async fn wait_for_time_sync() -> Result<()> {
        let mut okay: bool = false;

        while !okay {
            match local_time().await {
                Ok(time) => {
                    let offset = time - Local::now();
                    okay = offset < Duration::minutes(1);
                    info!("Got time {time} ({offset}, {okay})");
                }
                Err(e) => {
                    warn!("Error {e}");
                }
            }

            if !okay {
                sleep(tokio::time::Duration::from_secs(15)).await;
            }
        }

        Ok(())
    }
}