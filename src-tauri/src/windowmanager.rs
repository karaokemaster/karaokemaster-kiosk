use tauri::{
    AppHandle,
    plugin::{Builder, TauriPlugin},
    Manager,
    Runtime,
};

extern crate strfmt;
use strfmt::strfmt;
use std::collections::HashMap;
use tokio::sync::RwLock;
use regex::Regex;
use log::*;
use chrono::{Timelike, Utc};
use url::Url;

use crate::server::{KioskIdPayload, StatusPayload};
use crate::systeminfo::system_info::get_name;
use crate::state::ManagerExt;
use crate::built_info;

#[derive(Clone, serde::Serialize)]
struct SplashCompletePayload {
}

const INIT_SCRIPT: &str = r#"
      window.__KIOSK_ID__ = {kiosk_id};
      window.__KIOSK_DISPLAY_ID__ = {display_id};
      window.__KIOSK_NAME__ = '{name}';
    "#;

const INIT_SCRIPT_2: &str = r#"
      function kmTauriDocReady(fn) {{
        if (document.readyState === "complete" || document.readyState === "interactive") {{
          setTimeout(fn, 1);
        }} else {{
          document.addEventListener("DOMContentLoaded", fn);
        }}
      }}

      kmTauriDocReady(function () {{
        let cmd = "window_loaded";
        // let args = {{ kioskId: {kiosk_id}, displayId: {display_id}, label: "{label}" }};
        let args = {{}};

        window.__TAURI_IPC__({{
          cmd,
          callback: {display_id},
          error: {display_id},
          payload: args,
          options: {{}}
        }});
      }});
    "#;

pub struct WindowManager {
    app_handle: AppHandle,
    windows: RwLock<HashMap<i32, tauri::Window>>,
    splash_status: RwLock<HashMap<i32, bool>>,
    status: RwLock<String>,
    window_status: RwLock<HashMap<i32, String>>,
    use_fullscreen: bool,
    kiosk_id: RwLock<i32>,
}

pub fn plugin<R: Runtime>() -> TauriPlugin<R> {
    Builder::new("window_manager")
        .setup(move |app_handle| {
            let handle1 = app_handle.clone();
            let handle2 = app_handle.clone();
            let handle3 = app_handle.clone();
            let handle4 = app_handle.clone();
            let handle5 = app_handle.clone();

            app_handle.listen_global("splash-complete", move |_event| {
                let handle = handle1.clone();

                tauri::async_runtime::spawn(async move {
                    let windows = handle.get_window_manager().await.unwrap();
                    windows.set_status("loading-time").await;
                });
            });

            app_handle.listen_global("time-sync", move |_event| {
                let handle = handle2.clone();

                tauri::async_runtime::spawn(async move {
                    let windows = handle.get_window_manager().await.unwrap();
                    windows.set_status("loading-mdns").await;
                });
            });

            app_handle.listen_global("set-server-urls", move |_event| {
                let handle = handle3.clone();

                tauri::async_runtime::spawn(async move {
                    let windows = handle.get_window_manager().await.unwrap();
                    windows.set_status("loading-signalr").await;
                });
            });

            app_handle.listen_global("connected", move |_event| {
                let handle = handle4.clone();

                tauri::async_runtime::spawn(async move {
                    let windows = handle.get_window_manager().await.unwrap();
                    windows.set_status("loading-adopt").await;
                });
            });

            app_handle.listen_global("set-kiosk-id", move |event| {
                let handle = handle5.clone();
                let payload: KioskIdPayload = serde_json::from_str(event.payload().unwrap()).unwrap();

                tauri::async_runtime::spawn(async move {
                    let windows = handle.get_window_manager().await.unwrap();
                    windows.set_kiosk_id(payload.id).await;
                });
            });

            Ok(())
        })
        .build()
}

impl WindowManager {
    pub fn new(app_handle: AppHandle) -> Self {
        let window = tauri::WindowBuilder::new(&app_handle, "label", tauri::WindowUrl::App("index.html".into()))
            .visible(false)
            .build()
            .unwrap();

        debug!("Starting WindowManager constructor...");

        let monitors: Vec<tauri::Monitor> = window.available_monitors().unwrap().into();

        let mut windows: HashMap<i32, tauri::Window> = HashMap::new();
        let mut splash_status: HashMap<i32, bool> = HashMap::new();
        let window_status: HashMap<i32, String> = HashMap::new();

        let use_fullscreen = true; // built_info::DEBUG;

        for (pos, monitor) in monitors.iter().enumerate() {
            let display_id: i32 = (pos + 1) as i32;
            let label: String = format!("splash{display_id}").into();

            debug!("Creating window id {0} with label {1}...", display_id, label);

            let name = get_name().unwrap();

            let mut vars = HashMap::new();
            vars.insert("kiosk_id".to_string(), "-1".to_string());
            vars.insert("display_id".to_string(), display_id.to_string());
            vars.insert("name".to_string(), name);
            let script = strfmt(INIT_SCRIPT, &vars).unwrap();

            let position = monitor.position();

            let new_window = tauri::WindowBuilder::new(&app_handle, &label, tauri::WindowUrl::App("index.html".into()))
                .position(position.x.into(), position.y.into())
                .title("KaraokeMaster")
                .fullscreen(use_fullscreen)
                .transparent(use_fullscreen)
                .decorations(!use_fullscreen)
                .visible(false)
                .always_on_top(use_fullscreen)
                .initialization_script(&script)
                .build()
                .unwrap();

            new_window.set_cursor_visible(false).unwrap();

            debug!("Creating new window {2} with position {0} {1}", position.x, position.y, &label);

            new_window.set_position(tauri::Position::Logical(tauri::LogicalPosition { x: position.x.into(), y: position.y.into() }))
                .unwrap();

            windows.insert(display_id, new_window);

            splash_status.insert(display_id, false);
        }

        trace!("Closing temporary window...");
        
        window.close().unwrap();
        app_handle.emit_all("set-status", StatusPayload { display_id: 0, status: "loading".into() }).unwrap();
        // app_handle.trigger_global("set-status", StatusPayload { id: 0, status: "loading".into() }).unwrap();

        debug!("Completed WindowManager constructor");

        Self {
            app_handle,
            windows: RwLock::with_max_readers(windows, 1024),
            splash_status: RwLock::with_max_readers(splash_status, 1024),
            status: RwLock::with_max_readers("loading".into(), 1024),
            window_status: RwLock::with_max_readers(window_status, 1024),
            use_fullscreen,
            kiosk_id: RwLock::with_max_readers(-1, 1024),
        }
    }

    pub async fn set_kiosk_id(self: &Self, id: i32) {
        trace!("set_kiosk_id {id}");

        *self.kiosk_id.write().await = id;
    }

    pub async fn splash_complete(self: &Self, display_id: i32) {
        trace!("splash_complete for id {display_id}");

        let splash_status = &mut self.splash_status.write().await;

        if splash_status.contains_key(&display_id) {
            splash_status.remove(&display_id);
        }

        splash_status.insert(display_id, true);

        for loaded in splash_status.values() {
            if !loaded {
                debug!("Not all splash windows loaded; holding splash-complete event...");
                return;
            }
        }

        debug!("All splash windows loaded; sending splash-complete event...");
        self.app_handle.trigger_global("splash-complete", None);
        self.app_handle.emit_all("splash-complete", {}).unwrap();
    }

    pub async fn set_status(self: &Self, status: &str) {
        *self.status.write().await = status.into();

        self.send_window_statuses().await;
    }

    pub async fn set_window_status(self: &Self, display_id: i32, status: &str) {
        {
            let window_status = &mut self.window_status.write().await;

            trace!("got window_status lock");

            if window_status.contains_key(&display_id) {
                window_status.remove(&display_id);
            }

            window_status.insert(display_id, status.into());
        }

        trace!("Dropped window_status lock");

        let windows = self.windows.read().await;
        let window = windows.get(&display_id).unwrap();

        trace!("got window for {display_id}");

        let splash_label = format!("splash{display_id}");
        let all_windows = self.app_handle.windows();
        let splash_window = all_windows.get(&splash_label).unwrap();

        match splash_window.emit("set-status", StatusPayload { display_id, status: status.into() }) {
            Ok(()) => {
                trace!("Sent status {status} to splash {display_id}");
            },
            Err(e) => {
                warn!("Error {e}");
            }
        };

        if window.label() != splash_window.label() {
            match window.emit("set-status", StatusPayload { display_id, status: status.into() }) {
                Ok(()) => {
                    trace!("Sent status {status} to window {display_id}");
                },
                Err(e) => {
                    warn!("Error {e}");
                }
            };
        }
    }

    async fn send_window_statuses(self: &Self) {
        debug!("send_window_statuses starting...");

        let re = Regex::new(r"([a-z]*)(\d+)\D?\d*").unwrap();
        let windows = self.windows.read().await;
        trace!("windows!");
        let app_windows = self.app_handle.windows();
        trace!("app_windows!!");
        let all_status: String = self.status.read().await.as_str().into();
        trace!("all_status!!!");
        let window_status = self.window_status.read().await;
        trace!("window_status!!!!");

        trace!("windows.iter()");
        app_windows.iter()
            .for_each(|(label, app_window)| {
                trace!("window {label}");

                let caps = re.captures(label).unwrap();
                let display_id = caps.get(2).unwrap().as_str().parse::<i32>().unwrap();

                let window = windows.get(&display_id).unwrap_or(app_window);
                let label = window.label();
                if label.starts_with("content") && label != app_window.label() {
                    debug!("whatever {0}", label);
                    return ();
                }

                let status = window_status.get(&display_id).unwrap_or(&all_status);

                info!("Setting status for {label} to {status}...");

                match app_window.emit("set-status", StatusPayload { display_id, status: status.into() }) {
                    Ok(()) => { },
                    Err(e) => {
                        warn!("Error {e}");
                    }
                };
            });
    }

    pub async fn set_display_url(self: &Self, display_id: i32, url: String) {
        trace!("set display {display_id} url {url}");

        self.set_window_status(display_id, "loading-content").await;

        {
            let windows = &mut self.windows.write().await;
            let app_windows = self.app_handle.windows();
            let kiosk_id = self.kiosk_id.read().await;

            let splash_label = format!("splash{display_id}");
            let splash = app_windows.get(&splash_label).unwrap();
            let parsed_url: Url = url.parse().unwrap();

            let hide_splash = built_info::CFG_OS == "macos" && parsed_url.scheme() == "https";

            if !hide_splash {
                splash.show().unwrap();
            }

            let now = Utc::now();
            let uid = format!("{:02}{:02}{:02}", now.hour(), now.minute(), now.second());
            let label_base = format!("content{display_id}");
            let label = format!("{label_base}_{uid}");

            for (label, window) in app_windows.iter() {
                if label.starts_with(label_base.as_str()) {
                    window.close().unwrap();
                }
            }

            let mut vars = HashMap::new();
            vars.insert("kiosk_id".to_string(), format!("{}", kiosk_id));
            vars.insert("display_id".to_string(), format!("{}", display_id));
            vars.insert("label".to_string(), label.clone());
            let script = strfmt(INIT_SCRIPT_2, &vars).unwrap();

            let position = splash.outer_position().unwrap();

            self.app_handle.ipc_scope().configure_remote_access(
                tauri::ipc::RemoteDomainAccessScope::new(parsed_url.host_str().unwrap())
                    .add_window(&label)
                    .enable_tauri_api()
                );

            let new_window = tauri::WindowBuilder::new(&self.app_handle, label, tauri::WindowUrl::External(parsed_url.clone()))
                .position(position.x.into(), position.y.into())
                .title("KaraokeMasterContent")
                .fullscreen(self.use_fullscreen)
                .transparent(self.use_fullscreen)
                .decorations(!self.use_fullscreen)
                .focused(!hide_splash)
                .visible(true)
                .initialization_script(&script)
                .additional_browser_args("--autoplay-policy=no-user-gesture-required --disable-features=msWebOOUI,msPdfOOUI,msSmartScreenProtection")
                .build()
                .unwrap();

            new_window.set_cursor_visible(false).unwrap();

            if hide_splash {
                new_window.show().unwrap();
                splash.hide().unwrap();

                new_window.set_focus().unwrap();
            }

            // new_window.open_devtools();

            debug!("set display {display_id} url {url} complete");

            windows.remove(&display_id);
            windows.insert(display_id, new_window);
        }

        self.send_window_statuses().await;
    }
}

#[tauri::command]
pub async fn window_loaded(app: tauri::AppHandle, window: tauri::Window) {
    let label = window.label();
    info!("window {label} loaded");

    let re = Regex::new(r"([a-z]*)(\d+)\D?\d*").unwrap();
    let caps = re.captures(label).unwrap();
    let kind = caps.get(1).unwrap().as_str();
    let display_id = caps.get(2).unwrap().as_str().parse::<i32>().unwrap();

    debug!("window {display_id} {kind} loaded");

    let windows = app.get_window_manager().await.unwrap();

    trace!("Got window manager");

    window.show().unwrap();

    if kind == "content" {
        window.set_cursor_visible(false).unwrap();
        trace!("set_window_status({display_id}, \"load-complete\")");
        windows.set_window_status(display_id, "load-complete").await;

        window.set_focus().unwrap();
    }
    
    if kind == "splash" {
        trace!("splash_complete({display_id})");
        windows.splash_complete(display_id).await;
    }
    if kind == "content" && !windows.use_fullscreen {
        // TODO: Eliminate this block...
        let app_windows = app.windows();
        let splash_label = format!("splash{display_id}");

        debug!("closing {splash_label} window...");

        let splash = app_windows.get(&splash_label).unwrap();

        splash.hide().unwrap();
        window.set_focus().unwrap();

        info!("closed {splash_label} window!");
    }

    windows.send_window_statuses().await;

    trace!("window {display_id} {kind} load complete");
}

#[tauri::command]
pub async fn close_splash(_app: tauri::AppHandle, window: tauri::Window)
{
    debug!("closing splash window...");

    window.hide().unwrap();

    info!("closed splash window!");
}