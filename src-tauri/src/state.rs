use anyhow::Result;
use tauri::{Manager, Runtime};
use async_trait::async_trait;
use tokio::sync::Mutex;

use crate::windowmanager::WindowManager;
use crate::server::ServerConnection;

#[derive(Default)]
pub struct Windows(pub Mutex<Option<WindowManager>>);

#[derive(Default)]
pub struct Connection(pub Mutex<Option<ServerConnection>>);

#[async_trait]
pub trait ManagerExt<R: Runtime> : Manager<R> {
    async fn get_window_manager(&self) -> Result<&WindowManager> {
        let windows_state = self.state::<Windows>();
        let windows_ref = windows_state
                            .0
                            .lock()
                            .await;
        let windows = windows_ref
                            .as_ref()
                            .expect("Window manager not initialized!");

        unsafe {
            let owned_windows = extend_lifetime::extend_lifetime(windows);
            Ok(owned_windows)
        }
    }

    fn get_window_manager_blocking(&self) -> Result<&WindowManager> {
        let windows_state = self.state::<Windows>();
        let windows_ref = windows_state
                            .0
                            .blocking_lock();
        let windows = windows_ref
                            .as_ref()
                            .expect("Window manager not initialized!");

        unsafe {
            let owned_windows = extend_lifetime::extend_lifetime(windows);
            Ok(owned_windows)
        }
    }

    async fn get_server_connection(&self) -> Result<&ServerConnection> {
        let connection_state = self.state::<Connection>();
        let connection_ref = connection_state
                            .0
                            .lock()
                            .await;
        let connection = connection_ref
                            .as_ref()
                            .expect("Connection not initialized!");

        unsafe {
            let owned_connection = extend_lifetime::extend_lifetime(connection);
            Ok(owned_connection)
        }
    }
}

#[async_trait]
impl<R: Runtime, A: Manager<R>> ManagerExt<R> for A {}
