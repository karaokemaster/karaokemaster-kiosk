#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

pub mod windowmanager;
pub mod systeminfo;
pub mod server;
pub mod state;
pub mod built_info {
    // The file has been placed there by the build script.
   include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

use anyhow::Result;
use tauri::Manager;
use std::{env, process};
use log::*;
use url::Url;
use tracing_log::LogTracer;
use tracing_subscriber::{Registry, reload::{self, Handle}, fmt, filter::EnvFilter, layer::{Layer, SubscriberExt}};

use crate::windowmanager::{WindowManager, plugin as window_manager_plugin, window_loaded, close_splash};
use crate::systeminfo::system_info::{plugin as system_info_plugin, get_name};
use crate::server::{ServerConnection, ServerUrlPayload};
use crate::state::{Windows, Connection};

static mut LOKI_HANDLE: Option<Handle<Box<dyn Layer<Registry> + Send + Sync>, Registry>> = None;

#[tokio::main]
#[quit::main]
async fn main() -> Result<()> {
    LogTracer::init().expect("Unable to setup log tracer!");

    let (loki_layer, loki_reload_handle) = reload::Layer::new(tracing_subscriber::layer::Identity::new().boxed());
    unsafe {
        LOKI_HANDLE = Some(loki_reload_handle);
    }
    let console_layer = fmt::layer()
        .pretty()
        .with_ansi(true)
        .with_filter(EnvFilter::from_env("KM_LOG"));

    let subscriber = Registry::default()
        .with(fmt::Layer::new().with_filter(EnvFilter::new("info,karaokemaster_kiosk=trace")).and_then(loki_layer))
        .with(console_layer);
    tracing::subscriber::set_global_default(subscriber).unwrap();

    let _client = sentry::init((
        "https://8bfa4cb308514367b51a4559812b568b@o409012.ingest.sentry.io/4504432244948992",
        sentry::ClientOptions {
            release: sentry::release_name!(),
                    ..Default::default()
        },
    ));

    // Everything before here runs in both app and crash reporter processes
    // let _guard = sentry_tauri::minidump::init(&client);
    // Everything after here runs in only the app process

    tauri::Builder::default()
        .plugin(sentry_tauri::plugin())
        .plugin(system_info_plugin())
        .plugin(window_manager_plugin())
        .manage(Connection(Default::default()))
        .manage(Windows(Default::default()))
        .invoke_handler(tauri::generate_handler![window_loaded, close_splash])
        .setup(move |app| {
            let handle = app.handle();
            *handle.state::<Windows>().0.try_lock().unwrap() = Some(WindowManager::new(handle.clone()));
            *handle.state::<Connection>().0.try_lock().unwrap() = Some(ServerConnection::new(handle.clone()));

            app.listen_global("set-server-urls", |event| {
                trace!("eventhandler: set-server-urls");

                let server: ServerUrlPayload = serde_json::from_str::<ServerUrlPayload>(event.payload().unwrap()).unwrap();

                let loki_layer = setup_logger(server.loki_url)
                    .unwrap();
                unsafe {
                    (&LOKI_HANDLE).as_ref().unwrap().reload(loki_layer.boxed()).unwrap();
                }
            });

            trace!("Tauri app setup complete");

            Ok(())
        })
        .run(tauri::generate_context!())
        .expect("error while building tauri application");

    logger().flush();

    Ok(())
}

fn setup_logger(loki_url: String) -> Result<tracing_loki::Layer, tracing_loki::Error> {
    let url = Url::parse(loki_url.as_str()).unwrap();

    trace!("Loki url: {}", url);

    // TODO: Add more labels
    // TODO: Add category label for logger
    let (layer, task) = tracing_loki::builder()
        .label("job", "KaraokeMaster.Kiosk")?
        .label("instance", get_name().unwrap())?
        .label("version", format!("kmkiosk-{}", built_info::PKG_VERSION))?
        // .label("module", "")?
        .extra_field("pid", format!("{}", process::id()))?
        .build_url(url)?;

    // The background task needs to be spawned so the logs actually get
    // delivered.
    tokio::spawn(task);
    
    Ok(layer)
}