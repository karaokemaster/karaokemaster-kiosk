#!/bin/bash

releaseUrl="https://gitlab.com/karaokemaster/karaokemaster-kiosk/-/jobs/5974244044/artifacts/raw/karaoke-master-kiosk_0.10.0-alpha.31_armhf.deb"
splashUrl="https://gitlab.com/karaokemaster/karaokemaster-kiosk/-/raw/develop/install/linux/splash.png?inline=false"
wallpaperUrl="https://gitlab.com/karaokemaster/karaokemaster-kiosk/-/raw/develop/install/linux/wallpaper.png?inline=false"
emojiUrl="https://github.com/samuelngs/apple-emoji-linux/releases/download/v16.4-patch.1/AppleColorEmoji.ttf"
dependencies="libgtk-3-0 libwebkit2gtk-4.0-37 gldriver-test libnotify4 libnss3 libxss1 libxtst6 xdg-utils libatspi2.0-0 libuuid1 libappindicator3-1 libsecret-1-0 fbi xserver-xorg ratpoison x11-xserver-utils xinit xli libgtk-3-0 bc desktop-file-utils libavahi-compat-libdnssd1 libpam0g-dev libx11-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio"

echo "Installing Dependencies ..."
{
  sudo apt -qq update
  sudo apt -qq full-upgrade -y
  sudo apt -qq install $dependencies -y
} 

echo "Installing KaraokeMaster ..."
{
  wget -O karaokemaster.deb $releaseUrl -q --show-progress
  wget -O splash.png $splashUrl -q --show-progress
  wget -O wallpaper.png $wallpaperUrl -q --show-progress
  sudo dpkg -i karaokemaster.deb
  rm karaokemaster.deb
  sudo apt --fix-broken -qq install -y
}

echo "Installing Emoji font ..."
{
  wget -O AppleColorEmoji.ttf $emojiUrl -q --show-progress
  mv AppleColorEmoji.ttf ~/.local/share/fonts
  fc-cache -f -v
}

echo "Setting up Autostart ..."
cat <<EOF > ~/.xinitrc
#!/bin/sh

xset s off
xset s noblank
xset -dpms

ratpoison&
xli -center -onroot wallpaper.png&
karaoke-master-kiosk
EOF

cat <<EOF >> ~/.bashrc
if [ -z "\$SSH_CLIENT" ] || [ -z "\$SSH_TTY" ]; then
    xinit -- -nocursor
fi
EOF

cat <<EOF >> ~/.ratpoisonrc
startup_message off
EOF
}
echo "Setting up splash screen ..."
sudo bash -c 'cat >> /boot/config.txt' << EOF
disable_splash=1
EOF

#sudo sed -i 's/console=tty1/console=tty3 loglevel=0 logo.nologo quiet splash/' /boot/cmdline.txt

sudo bash -c 'cat >> /etc/systemd/system/splashscreen.service' << EOF
[Unit]
Description=Splash Screen
After=local-fs.target

[Service]
ExecStart=/usr/bin/fbi -d /dev/fb0 --noverbose -a $(pwd)/splash.png
Type=oneshot
StandardInput=tty
StandardOutput=tty

[Install]
WantedBy=sysinit.target
EOF

sudo systemctl enable splashscreen

echo "Setting up audio ..."
cat <<EOF > ~/disable_hdmi_audio.dts
/dts-v1/;
/plugin/;
/ {
	compatible = "brcm,bcm2835";
	fragment@0 {
		target = <&audio>;
		__overlay__ {
			brcm,disable-hdmi = <1>;
		};
	};
};
EOF
sudo dtc -I dts -O dtb -o /boot/overlays/disable_hdmi_audio.dtbo ~/disable_hdmi_audio.dts

sudo bash -c 'cat >> /boot/config.txt' << EOF
dtoverlay=disable_hdmi_audio
dtoverlay=hifiberry-dacplus,slave
force_eeprom_read=0
EOF

echo "Setting up Console Autologin ..."
sudo systemctl set-default multi-user.target
sudo ln -fs /lib/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@tty1.service
#sudo systemctl enable getty@tty1.service
sudo mkdir /etc/systemd/system/getty@tty1.service.d
sudo bash -c 'cat > /etc/systemd/system/getty@tty1.service.d/autologin.conf' << EOF
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin $USER --noclear %I \$TERM
EOF

echo "Setting up time sync ..."
sudo sed -i 's/#NTP=/NTP=ntp.cheznorah.com/' /etc/systemd/timesyncd.conf

echo "Setting Permissions ..."
sudo chmod +x ~/.xinitrc
sudo chmod ug+s /usr/lib/xorg/Xorg
