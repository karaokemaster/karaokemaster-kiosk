Param(
    [string]$UserName,
    [string]$PicturePath
)

# Get identifiers for path components
$sid = [System.Security.Principal.NTAccount]::new($UserName).Translate([System.Security.Principal.SecurityIdentifier]).ToString()
$pictureGuid = [guid]::NewGuid().ToString().ToUpper()

# Load the new image
Add-Type -AssemblyName System.Drawing
$picture = [System.Drawing.Image]::FromFile((gi $PicturePath).FullName)

# Create or gain access to the AccountPictures subfolder
$picturesFolder = Join-Path (Join-Path $env:PUBLIC 'AccountPictures') $sid
If (Test-Path $picturesFolder) {
    Push-Location $picturesFolder
    takeown /f . /a | Out-Null
    icacls . /grant 'Administrators:(OI)(CI)F' | Out-Null
    Pop-Location
} Else {
    mkdir $picturesFolder | Out-Null
    Push-Location $picturesFolder
    icacls . /grant 'Everyone:(OI)(CI)R' | Out-Null
    Pop-Location
}

# Create or gain access to the picture Registry key
$picturesKey = Join-Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\AccountPicture\Users $sid
If (Test-Path $picturesKey) {
    $keySubpath = "SOFTWARE\Microsoft\Windows\CurrentVersion\AccountPicture\Users\$sid"
    $keyObject = [Microsoft.Win32.RegistryKey]::OpenBaseKey('LocalMachine', 'Registry64').OpenSubkey($keySubpath, 'ReadWriteSubTree', 'ChangePermissions')
    $acl = $keyObject.GetAccessControl()
    $acl.AddAccessRule([System.Security.AccessControl.RegistryAccessRule]::new('Administrators', 'FullControl', 'ContainerInherit', 'None', 'Allow'))
    $keyObject.SetAccessControl($acl)
    $keyObject.Dispose()
} Else {
    mkdir $picturesKey | Out-Null
}

# Prepare the JPG encoder
$encoder = [System.Drawing.Imaging.ImageCodecInfo]::GetImageEncoders() | ? { $_.MimeType -eq 'image/jpeg' } | select -First 1
$encoderParams = [System.Drawing.Imaging.EncoderParameters]::new()
$encoderParams.Param[0] = [System.Drawing.Imaging.EncoderParameter]::new([System.Drawing.Imaging.Encoder]::Quality, 90)

# Create resized versions of the picture
(32, 40, 48, 64, 96, 192, 208, 240, 424, 448, 1080) | % {
    $picturePath = Join-Path $picturesFolder "{$pictureGuid}-Image$_.jpg"
    $resized = [System.Drawing.Bitmap]::new($_, $_)
    $graphics = [System.Drawing.Graphics]::FromImage($resized)
    $graphics.InterpolationMode = [System.Drawing.Drawing2D.InterpolationMode]::HighQualityBicubic
    $graphics.DrawImage($picture, 0, 0, $_, $_)
    $resized.Save($picturePath, $encoder, $encoderParams)
    $resized.Dispose()
    Set-ItemProperty $picturesKey -Name "Image$_" -Value $picturePath
}