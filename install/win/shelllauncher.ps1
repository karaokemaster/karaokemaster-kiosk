﻿. .\ShellLauncherBridgeWmiHelpers.ps1

$src = (Resolve-Path $PSScriptRoot).Path

$sid = Get-LocalUser -Name "karaoke" | Select-Object -ExpandProperty SID
$profileId = New-Guid

$installPath = Get-ChildItem HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall | % { Get-ItemProperty $_.PsPath } | Where-Object -Property DisplayName -EQ "KaraokeMaster Kiosk" | Select -ExpandProperty InstallLocation
$shellPath = Join-Path $installPath "KaraokeMaster Kiosk.exe"

$launcherPath = (Join-Path $src ".\ShellLauncher.xml")
$xml = [xml](Get-Content ($launcherPath + ".template"))

$xml.ShellLauncherConfiguration.Configs.Config.Account.Sid = $sid.ToString()
$xml.ShellLauncherConfiguration.Configs.Config.Profile.Id = $profileId.ToString("B")
$xml.ShellLauncherConfiguration.Profiles.Profile.Id = $profileId.ToString("B")
$xml.ShellLauncherConfiguration.Profiles.Profile.Shell.Shell = $shellPath.ToString()

$xml.Save($launcherPath)

Set-ShellLauncherBridgeWmi -FilePath $launcherPath
