$key = "N4G7P-YJHJ2-P63PX-B62FQ-KKXR4"
$username = "karaoke"

### Activate Windows
Write-Host "Checking Windows Activation status..."
$windows = Get-CimInstance SoftwareLicensingProduct -Filter 'Name LIKE "Windows%" and PartialProductKey LIKE "%"'

if ( $windows.LicenseStatus -ne 1) {
    Write-Host "Activating Windows..."
    $service = Get-WmiObject -ComputerName "." -Class SoftwareLicensingService
    $service.InstallProductKey($key)
    $service.RefreshLicenseStatus()

    Invoke-CimMethod -MethodName Activate -InputObject $windows
} else {
    Write-Host "Windows already activated"
}

### Create kiosk user
$UserID = Get-LocalUser -Name $username -ErrorAction SilentlyContinue
$password = ConvertTo-SecureString -String $username -AsPlainText -Force
if($UserID) {
    Write-Host "$username User Found"
} else {
    Write-Host "Creating $username user..."
    New-LocalUser $username -Password $password | Out-Null
}

### Set lockscreen
Write-Host "Set lock screen wallpaper..."
$wallpaperPath = Join-Path $PWD "wallpaper.png"
.\Set-LockScreen.ps1 -LockScreenSource $wallpaperPath

### Set user wallpaper
Write-Host "Setting user wallpaper..."
$credential = New-Object System.Management.Automation.PSCredential $username, $password
$lockScreenScript = Join-Path $PWD "Set-LockScreen.ps1"
$userPath = (Get-CimInstance Win32_UserProfile -Filter "SID = '$((Get-LocalUser $username).Sid)'").LocalPath
$userWallpaperPath = Join-Path $userPath "wallpaper.png"
Copy-Item $wallpaperPath -Destination $userWallpaperPath -Force
Start-Process "PowerShell.exe -ExecutionPolicy Bypass -File $lockScreenScript -BackgroundSource $userWallpaperPath" -Credential $credential

### Set user properties
Write-Host "Setting user properties..."
Set-LocalUser -Name $username -FullName "KaraokeMaster" -Description "KaraokeMaster kiosk user" -PasswordNeverExpires 1
.\Set-AccountPicture.ps1 -UserName $username -PicturePath .\logo.png

### Set theme
Write-Host "Setting theme..."
Start-Process -FilePath "C:\Windows\Resources\Themes\themeB.theme"

### Disable power save
Write-Host "Disabling power save mode..."
powercfg -change -monitor-timeout-ac 0
powercfg -change -standby-timeout-ac 0

### Configure shell launcher
Write-Host "Configure kiosk mode..."
$launcher = Join-Path $PWD "shelllauncher.cmd"
.\psexec.exe -i -s $launcher

### Configure auto-login
Write-Host "Configure auto-login..."
$RegPath = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
Set-ItemProperty $RegPath "AutoAdminLogon" -Value "1" -type String 
Set-ItemProperty $RegPath "DefaultUsername" -Value $username -type String 
Set-ItemProperty $RegPath "DefaultPassword" -Value $username -type String

### Enable remote desktop
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 0
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"

### Add firewall exceptions
$installPath = Get-ChildItem HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall | % { Get-ItemProperty $_.PsPath } | Where-Object -Property DisplayName -EQ "KaraokeMaster Kiosk" | Select -ExpandProperty InstallLocation
$exePath = Join-Path $installPath "KaraokeMaster Kiosk.exe"
New-NetFirewallRule -Action Allow -Group "KaraokeMaster Kiosk" -DisplayName "KaraokeMaster Kiosk (Outbound TCP)" -Direction Outbound -PolicyStore PersistentStore -Profile Any -Program $exePath -Protocol Tcp
New-NetFirewallRule -Action Allow -Group "KaraokeMaster Kiosk" -DisplayName "KaraokeMaster Kiosk (Outbound UDP)" -Direction Outbound -PolicyStore PersistentStore -Profile Any -Program $exePath -Protocol Udp
New-NetFirewallRule -Action Allow -Group "KaraokeMaster Kiosk" -DisplayName "KaraokeMaster Kiosk (Inbound TCP)" -Direction Inbound -PolicyStore PersistentStore -Profile Any -Program $exePath -Protocol Tcp
New-NetFirewallRule -Action Allow -Group "KaraokeMaster Kiosk" -DisplayName "KaraokeMaster Kiosk (Inbound UDP)" -Direction Inbound -PolicyStore PersistentStore -Profile Any -Program $exePath -Protocol Udp

### Set UEFI boot logo
Write-Host "Set UEFI boot logo..."
.\HackBGRT\setup.exe

Write-Host "DONE!"