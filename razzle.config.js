'use strict';
module.exports = {
    options: {
        buildType: 'spa',
    },
    modifyWebpackConfig(opts) {
        const config = opts.webpackConfig;

        config.output.hashFunction = 'sha256';

        return config;
    },
};