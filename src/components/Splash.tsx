import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleCheck, faExclamationTriangle, faHourglass, faInfoCircle } from '@fortawesome/pro-light-svg-icons';
import { faHourglassClock } from '@fortawesome/pro-regular-svg-icons';
import { faClock, faRadar, faHouseSignal, faGear } from '@fortawesome/pro-solid-svg-icons';
import { faCircleNotch } from '@fortawesome/pro-thin-svg-icons';
import { listen, Event } from '@tauri-apps/api/event'
import { invoke } from '@tauri-apps/api/tauri'
import React, { useState, useEffect } from 'react';
import icon from '../../assets/icon300.png';

import './Splash.css';

interface KioskIdPayload {
    id: number;
}
interface DisplayIdPayload {
    id: number;
}
interface StatusPayload {
    displayId: number;
    status: string;
}

const Splash = () => {
    const [status, setStatus] = useState('waiting');
    const [kioskId, setKioskId] = useState(-1);
    const [displayId, setDisplayId] = useState(-1);
    const [displayName, setDisplayName] = useState('');

    useEffect(() => {
        const kioskId = (window as any).__KIOSK_ID__;
        const displayId = (window as any).__KIOSK_DISPLAY_ID__;
        const name = (window as any).__KIOSK_NAME__;

        const onLoad = () => {
            setTimeout(() => invoke('window_loaded'), 500);
        }

        const unlisten0 = listen('set-kiosk-id', (event: Event<KioskIdPayload>) => {
            console.log(`kiosk id ${event.payload.id}`);
            setKioskId(event.payload.id);
        });
        const unlisten1 = listen('set-display-id', (event: Event<DisplayIdPayload>) => {
            console.log(`display id ${event.payload.id}`);
            setDisplayId(event.payload.id);
        });
        const unlisten2 = listen('set-status', (event: Event<StatusPayload>) => {
            console.log(`status ${event.payload.status} for window ${event.payload.displayId} ${displayId}`);
            if (displayId === event.payload.displayId || event.payload.displayId === 0)
                setStatus(event.payload.status);
        });

        setKioskId(kioskId);
        setDisplayId(displayId);
        setDisplayName(name);

        if (document.readyState === 'complete') {
            onLoad();
        }
        else {
            window.addEventListener('load', onLoad);
        }

        return () => {
            window.removeEventListener('load', onLoad);
            unlisten0.then(unsub => unsub());
            unlisten1.then(unsub => unsub());
            unlisten2.then(unsub => unsub());
        };
    }, []);

    useEffect(() => {
        if (status === 'load-complete') {
            document.body.classList.add('fade');

            // TODO: use animationend event instead
            setTimeout(async () => invoke('close_splash'), 4000);
            setTimeout(async () => document.body.classList.remove('fade'), 5000);
        }
        else {
            // document.body.classList.remove('fade');
        }
    }, [status]);

    let statusIcon;
    let stage = 0;

    switch (status) {
        case 'error':
            statusIcon = faExclamationTriangle;
            break;
        case 'loading-time':
            statusIcon = faClock;
            stage = 0;
            break;
        case 'loading-mdns':
            statusIcon = faRadar;
            stage = 1;
            break;
        case 'loading-signalr':
            statusIcon = faHouseSignal;
            stage = 2;
            break;
        case 'loading-adopt':
            statusIcon = faGear;
            stage = 3;
            break;
        case 'loading-content':
            statusIcon = faHourglassClock;
            stage = 4;
            break;
        case 'load-complete':
            statusIcon = faCircleCheck;
            stage = 5;
            break;
        default:
            statusIcon = faHourglass;
            stage = -1;
            break;
    }

    // TODO: calculate icon height... width * 0.278

    return (
        <div className="root">
            <div className="background">&nbsp;</div>
            <div>&nbsp;</div>
            <div className="icon">
                <img width="300px" src={icon} alt="icon"/>
            </div>
            <div className="status">
                <span className="fa-layers fa-fw fa-2xl">
                    <FontAwesomeIcon icon={statusIcon} transform="shrink-9" />
                    <FontAwesomeIcon icon={faCircleNotch} spin/>
                </span>
            </div>
            <div className="info">
                <FontAwesomeIcon icon={faInfoCircle}/> {displayName} {kioskId}:{displayId}
            </div>
        </div>
    );
};

export default Splash;
